package com.example.listado.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.listado.databinding.RecyclerItemBinding
import com.example.listado.models.SOAndroid

class AdapterOS(private val listOS:List<SOAndroid>): RecyclerView.Adapter<AdapterOS.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterOS.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: RecyclerItemBinding = RecyclerItemBinding.inflate(layoutInflater, parent,false)
        return ViewHolder(binding)
    }




    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = listOS[position]
        with(holder.binding) {
            image.setImageResource(currentItem.icono)
            title.text = currentItem.titulo
            version.text = currentItem.version
        }
    }

    override fun getItemCount():Int = listOS.size

    inner class ViewHolder(val binding: RecyclerItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: SOAndroid) {
            binding.apply {
                title.text = item.titulo
                version.text= item.version

            }
        }
    }

}