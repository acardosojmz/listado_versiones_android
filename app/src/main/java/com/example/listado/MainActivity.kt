package com.example.listado

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

import androidx.recyclerview.widget.LinearLayoutManager
import com.example.listado.databinding.ActivityMainBinding
import com.example.listado.models.SOAndroid
import com.example.listado.ui.AdapterOS


class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val layoutManager = LinearLayoutManager(this)
        binding.recyclerView.setLayoutManager(layoutManager)
        val listOS=loadListOS()
        val adapter= AdapterOS(listOS)

        binding.recyclerView.adapter= adapter

    }
    fun loadListOS():List<SOAndroid>{
        val listOS = ArrayList<SOAndroid>()
        listOS.add(SOAndroid(R.drawable.cupcake,"Cupcake","1.5"))
        listOS.add(SOAndroid(R.drawable.donut,"DoNut","1.6"))
        listOS.add(SOAndroid(R.drawable.eclair,"Eclair","2.0 - 2,1"))
        listOS.add(SOAndroid(R.drawable.froyo,"Froyo","2.2 - 2.2.3" ))
        listOS.add(SOAndroid(R.drawable.gingerbread,"Gingerbread","2.3 - 2.3.7"))
        listOS.add(SOAndroid(R.drawable.honeycomb,"HoneyComb","3.0 - 3.2.6"))
        listOS.add(SOAndroid(R.drawable.icecream,"IceCream","4.0 - 4.0.5"))
        listOS.add(SOAndroid(R.drawable.jellybean,"JellyBean","4.1 - 4.3.1"))
        listOS.add(SOAndroid(R.drawable.kitkat,"Kitkat","4.4 – 4.4.4"))
        listOS.add(SOAndroid(R.drawable.lollipop,"Lollipop","5.0 – 5.1.1"))
        listOS.add(SOAndroid(R.drawable.marshmallow,"MarshMallow","6.0 – 6.0.1"))
        listOS.add(SOAndroid(R.drawable.nougat,"Nougat","7.0 – 7.1.2"))
        listOS.add(SOAndroid(R.drawable.oreo,"Oreo","8.0 – 8.1"))
        listOS.add(SOAndroid(R.drawable.pie,"Pie","9.0"))
        listOS.add(SOAndroid(R.drawable.diez,"Android 10","10.0"))
        listOS.add(SOAndroid(R.drawable.once,"Android 11","11.0"))

        binding.progressBar.visibility= View.GONE

        return listOS
    }
    
}


